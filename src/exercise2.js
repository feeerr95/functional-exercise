const faverage = numbers => {
  if(!numbers) return 0;
  if(numbers.length === 0) return 0;
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  return numbers.reduce(reducer) / numbers.length;
};

module.exports = faverage;
