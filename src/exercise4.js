const wordCount = sequence => {
  if(!sequence) return null;
  
  sequence = sequence.toLowerCase().split(' ').filter(word => word != '')
  
  if(sequence.length === 0)
    return null;

  return sequence.reduce((counter, currentValue) => 
    (counter[currentValue] = (counter[currentValue] || 0) + 1, counter), {});
  
};

module.exports = {
  wordCount,
};
